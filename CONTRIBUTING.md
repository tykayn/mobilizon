# Contributing to Mobilizon

---

## How we collaborate

First off, thank you for considering contributing to Mobilizon!

Our aim is for this project to make you feel welcome as a contributor. We hugely value the comments and contributions of community members in the various publicly-accessible areas in use, which currently are:

* Home: [Home](https://joinmobilizon.org)
* Wiki: [Project Wiki](https://framagit.org/framasoft/mobilizon/wikis) (Gitlab)
* Code: [Framagit](https://framagit.org/framasoft/mobilizon) (Gitlab)
* Issue tracking: [Framagit](https://framagit.org/framasoft/mobilizon) (Gitlab)
* Discussion: [Mobilizon forum](https://framacolibri.org/c/mobilizon) (Discourse)
* Chat: [#Mobilizon:matrix.org](https://matrix.org/#/room/#Mobilizon:matrix.org) (Matrix) or `#mobilizon` on Freenode (IRC)

### How to communicate with others ?

As you may know, English is not the only language in the community and communicating in various languages can be quite tricky. Some might not understand what you're saying, while other have a hard time writing in a language that is not their primary language.

If you don't have noticed yet, French is the de-facto language for communication in various channels. This is because a huge part of our community speaks French, as Framasoft is a French organization.

We don't want to impose French as the only language in the project, though, because we would lose a ton of potential users users and contributors that can help us make Mobilizon better.

You can write what you would like to tell us in the language you're most comfortable with and then use automagic translation to translate your message in English. It's far from perfect, but if we can understand the main idea you wanted to expose, it's sufficient.

Of course, you can and should express yourself directly in English if you're confident doing so!

### Other useful information

* It's early days for Mobilizon, so we're focused on laying down the foundations for the project. This means that you won't find much usable code yet.
* We're working as openly and transparently as possible with this project. As a contributor or member of the Mobilizon community, almost everything you come across will link to some other things that you should have access to. If you don't have access, and you think you should, just ask! 

### Contributopia

This project is part of [Framasoft](https://framasoft.org)'s [Contributopia Campaign](https://contributopia.org), creating and offering tools designed with a different set of values.

![Framameet illustration on Contributopia Website](https://contributopia.org/img/services-framameet.jpg)

### We practice [Ethical Design](https://2017.ind.ie/ethical-design/)

We endeavour to build technology that respects human rights, human effort, and human experience, and hope you will join in this effort.

[![Ethical Design diagram](https://i.imgur.com/O7RJo60.png)](https://2017.ind.ie/ethical-design/)

---
## Code of Conduct

This section explains Framasoft's commitment towards contributors to the [Mobilizon Project](https://joinmobilizon.org), as well as expectations that community members should have of one another. It is based on the [Contributor Covenant Code of Conduct](https://www.contributor-covenant.org/version/1/4/code-of-conduct) v1.4.

### Pledge

In the interest of fostering an open and welcoming environment, we as contributors and maintainers - including Framasoft - pledge to making participation in our project and our community a harassment-free experience for everyone, regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.

### Our Standards

Examples of behaviour that contributes to creating a positive environment include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behaviour by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others’ private information, such as a physical or electronic address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a professional setting

### Our Responsibilities

The Project maintainers are responsible for clarifying the standards of acceptable behaviour and are expected to take appropriate and fair corrective action in response to any instances of unacceptable behaviour.

Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, or to ban temporarily or permanently any contributor for other behaviours that they deem inappropriate, threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces when an individual is representing the project or its community. Examples of representing a project or community include using an official project e-mail address, posting via an official social media account, or acting as an appointed representative at an online or offline event. Representation of a project may be further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behaviour may be reported by contacting the project team at tcit plus mobilizon at framasoft dot org. All complaints will be reviewed and investigated and will result in a response that is deemed necessary and appropriate to the circumstances. The project team is obligated to maintain confidentiality with regard to the reporter of an incident. Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good faith may face temporary or permanent repercussions as determined by other members of the project’s leadership.

---

## How Can I Contribute

### Submitting Feature Requests, Enhancement Suggestions or Bug Reports

This section guides you through submitting a :sparkles: feature request, :lipstick: enhancement suggestions, and :bug: bug reports for Mobilizon - anything from errors and crashes, to minor improvements, to completely new features.

When you post an issue, please include as many details as possible. **Fill in the issue template** (available below) to help us resolve issues faster.

### Before making a submission

Please go through the checklist below before posting any :sparkles: :lipstick: :bug:

* **Check if you're using the latest version** of Mobilizon and all its relevant components and if you can get the desired behaviour by changing some config settings.
* **Perform a cursory search** in the issue tracker to see if the enhancement has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.
* Never report security related issues, vulnerabilities or bugs including sensitive information to the issue tracker, or elsewhere in public. Instead sensitive bugs must be sent by email to tcit plus mobilizon at framasoft dot org.

> **Note:** If you find a **Closed** issue that seems like it is the same thing that you're experiencing, open a new issue and include a link to the original issue in the body of your new one.

### Suggesting Features & Enhancements

If you find yourself wishing for a feature that doesn't exist in Mobilizon, you are probably not alone. There are probably others out there with similar needs.

Open an issue providing the following information:

* **Use a clear and descriptive title** for the issue to identify the suggestion.
* **Provide a description of the suggested enhancement** in as many details as possible, including the steps that you imagine you (as a user) would take if the feature you're requesting existed.
* **Describe the current behaviour** and **explain which behaviour you would like to see instead** and why.
* **Include screenshots and animated GIFs** which help you demonstrate the steps or point out the parts of Mobilizon which the suggestion is related to. You can use a tool called [LICEcap](https://www.cockos.com/licecap/) to record GIFs on macOS and Windows, and [Silentcast](https://github.com/colinkeenan/silentcast) or [Byzanz](https://github.com/GNOME/byzanz) on Linux.
* **Provide specific examples to demonstrate the enhancements**. If possible, include drawings, screenshots, or gifs of similar features in another app.
* **Explain why this enhancement would be useful** to most participants of Mobilizon and isn't something that can or should be implemented as a community extension.
* **List some other communities, platforms or apps where this enhancement exists.**

### Reporting Bugs :bug:

Open an issue providing the following information by filling in issue template below, explaining the problem and including additional details to help maintainers reproduce the problem:

* **Use a clear and descriptive title** for the issue to identify the problem.
* **Describe the exact steps which reproduce the problem** in as many details as possible. For example, start by explaining where you started in Mobilizon, and then which actions you took. When listing steps, **don't just say what you did, but explain how you did it**. For example, if you moved the cursor to the end of a line, explain if you used the mouse or a keyboard shortcut, and if so which one?
* **Provide specific examples to demonstrate the steps**. Include links to pages, screenshots, or copy/pasteable snippets, which you use in those examples. If you're providing snippets in the issue, use Markdown code blocks.
* **Describe the behaviour you observed after following the steps** and point out what exactly is the problem with that behaviour.
* **Explain which behaviour you expected to see instead and why.**
* **Include screenshots and animated GIFs** which show you following the described steps and clearly demonstrate the problem. You can use [this tool](https://www.cockos.com/licecap/) to record GIFs on macOS and Windows, and [this tool](https://github.com/colinkeenan/silentcast) or [this tool](https://github.com/GNOME/byzanz) on Linux.
* **If you're reporting a crash**, include a crash report with error logs. 
* **If the problem wasn't triggered by a specific action**, describe what you were doing before the problem happened and share more information using the guidelines below.

Provide more context by answering these questions:

* **Did the problem start happening recently** (e.g. after updating to a new version) or was this always a problem?
* If the problem started happening recently, **can you reproduce the problem in an older version?** What's the most recent version in which the problem doesn't happen? 
* **Can you reliably reproduce the issue?** If not, provide details about how often the problem happens and under which conditions it normally happens.
* If the problem is related to an event, comment or actor (eg. user or group), **does the problem happen for all of them or only some?** Does the problem happen only when working with local (originating from your Mobilizon instance) or remote ones, with anything specific about the object? Is there anything else special about the actors or events in question?

Include details about your configuration and environment:

* **Which version of each component are you using?** 
* **What's the name and version of the OS you're using**?
* **Are you running in a virtual machine?** If so, which VM software are you using and which operating systems and versions are used for the host and the guest?

---

### Template for submitting issues

#### Description

[Description of the issue]

#### Steps to Reproduce

1. [First Step]
2. [Second Step]
3. [and so on...]

#### Expected behaviour:

[What you expect to happen]

#### Actual behaviour:

[What actually happens]

#### Reproduces how often:

[What percentage of the time does it reproduce?]

#### Versions

[What Mobilizon instance you're using, and the versions of each relevant app or component, including your OS and browser.]

#### Additional Information

[Any additional information, configuration or data that might be necessary to reproduce the issue.]

---

### Contributing 

A common misconception about contributing to free and open source projects is that you need to contribute code. In fact, it’s often the other parts of a project that are most overlooked. You’ll do the project a huge favour by offering to pitch in with these types of contributions!

Even if you like to write code, other types of contributions are a great way to get involved with a project and meet other community members. Building those relationships may open up unexpected opportunities.

#### Do you like to design? :art:

* Restructure layouts to improve the project's usability
* Conduct user research to reorganise and refine the project's navigation or menus
* Create art for icons and app screens

#### Do you like to write? :pencil2:

* Write and improve the project's documentation
* Write tutorials for the project
* Curate a wiki page of examples showing how the project can be used

#### Do you like organising? :inbox_tray:

* Link to duplicate issues, and suggest new issue labels, to keep things organised
* Go through open issues and suggest revisiting or closing old ones
* Ask clarifying questions on recently opened issues to move the discussion forward

#### Do you like helping people? :raising_hand:

* Answer questions about the project on forums and other sites
* Answer questions for people on open issues

#### Do you like helping others code? :open_hands:

* Review code on other people’s submissions
* Write tutorials for how a project can be used
* Offer to mentor another contributor

#### Do you like to code? :nut_and_bolt:

* Find an open issue to tackle
* Offer to help write a new feature
* Improve tooling, testing & deployment options
* Read the **next section for guidelines**

---

### Contributing Code

Unsure where to begin contributing? You can start by looking through issues tagged with:

* [`first-timers-only`](https://www.firsttimersonly.com)- issues which should only require a few lines of code, and a test or two.
* `help-wanted` - issues which should be a bit more involved than `first-timers-only` issues.

#### Local development

Mobilizon can be developed locally. For instructions on how to do this, please see [the documentation](https://framagit.org/framasoft/mobilizon/wikis/install).

#### Coding & git practices

* We use GitLab's merge requests as our code review tool
* We encourage any dev to comment on merge requests and we think of the merge request not as a "please approve my code" but as a space for co-developing.
* We develop features on separate branches identified by issue numbers.
* We use merge to `develop` (not rebase) so that commits related to an issue can be retroactively explored.
* We don't currently use release branches or tags because we don't have release management at this phase of development.

#### How to make changes

* Make your changes on a seperate branch which includes an issue number e.g. `1234-some-new-feature` where 1234 is the issue number where the feature is documented. Make sure the branch is based on `develop`.
* Do not commit changes to files that are irrelevant to your feature or bugfix.
* Use commit messages descriptive of your changes.
* Push to the upstream of your new branch.
* Create a merge request at GitLab.

#### Git commit messages

* Limit the first line to 72 characters or less, referencing relevant issue numbers
* Be as descriptive as you want after the first line
* Consider starting the commit message with an applicable emoji (see Issue & Commit Categories below)

#### Merge requests

* Follow the code styleguides (TBD).
* Document new code based on the documentation styleguide (TBD)
* Each merge request should implement ONE feature or bugfix. If you want to add or fix more than one thing, submit more than one merge request.
* Fill in the merge request template below
* Include relevant issue number(s) in the merge request title
* Include screenshots or animated GIFs in your merge request whenever possible.
* End all files with a newline

#### Template for merge requests

* Description of the change
* Applicable issue numbers
* Alternate designs/implementations
* Benefits of this implementation
* Possible drawbacks
* Why should this be part of a core component?
* Testing process

---

#### Issue & Commit Categories

* :ambulance: `critical` : Critical hotfix!
* :lipstick: `enhancement` : General improvements.
* :sparkles: `feature` : New features.
* :bug: `bug` : Confirmed bugs, or reports that are likely to be bugs.
* :raising_hand: `question` : Questions (e.g. how can I do X?)
* :postbox: `feedback` : General feedback.
* :art: `ui` : Visual design.
* :scroll: `copy` : Text in the apps, or translations.
* :information_source: `documentation` : Documentation.
* :racehorse: `performance` : Performance.
* :lock: `security` : Security.
* :electric_plug: `api` : Mobilizon's APIs.
* :alien: `external` : External libraries or API integrations.
* :warning: `exception` : Uncaught exceptions.
* :fire: `crash` : Crash.
* :symbols: `encoding` : Character encoding or data serialization issue.
* :truck: `cleanup` : Removing, moving or refactoring code or files.
* :white_check_mark: `tests` : Testing

#### Issue Status

* :speech_balloon: `discussion` : Discussion to clarify this issue is ongoing.
* :soon: `todo` : This has been discussed and now needs work.
* :repeat: `needs-more-info` : More information needs to be collected about these problems or feature requests (e.g. steps to reproduce).
* :bulb: `idea` : Needs to be discussed further. Could be a feature request which might be good to first implement as a community extension.
* :construction: `in-progress` : Someone is working on this...
* :pray: `help-wanted` : The Mobilizon core team would appreciate help from the community in resolving these issues.
* :seedling: `first-timers-only` : Less complex issues which would be good first issues to work on for users who want to contribute.
* :1234: `needs-reproduction` : Likely bugs, but haven't been reliably reproduced.
* :red_circle: `blocked` : Blocked on other issues.
* :two: `duplicate` : Duplicate of another issue, i.e. has been reported before.
* :no_good: `wontfix` : The Mobilizon core team has decided not to fix these issues (or add these features) for now, because they're working as intended, or some other reason.
* :put_litter_in_its_place: `invalid` : Issues which are not valid (e.g. spam or submitted by error).


#### Merge Request Status

* :construction: `in-progress` : Still being worked on, more changes will follow.
* :busstop: `needs-review` : Needs code review and approval from maintainers.
* :mag: `under-review` : Being reviewed by maintainers.
* :wrench: `changes-required` : Needs to be updated based on review comments and then reviewed again.
* :eyes: `needs-testing` : Needs manual testing.

---

## Credits
The following documents have greatly helped us put this together. A big thank you to their authors and contributors!

* [MoodleNet's own Contributing document](https://gitlab.com/moodlenet/project/meta/issues/16)
* [Contributor Covenant](https://www.contributor-covenant.org/version/1/4/code-of-conduct)
* [Open Source Guides](https://opensource.guide)
* [Holochain's Development Protocols](https://github.com/holochain/holochain-proto/wiki/Development-Protocols)
* [Atom's contributing guide](https://github.com/atom/atom/blob/master/CONTRIBUTING.md)
* [Funkwhale's pad on how to Communicate with others](https://hackmd.io/qESHvdHZSWuhLNjeanaVQw?both)